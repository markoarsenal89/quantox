/**
 * Global namespace
 */
var APP = APP || {};


(function () {
	// Init routes
	APP.routes.init('.js-routes');
	// Init map
	APP.map.init('.js-map');

	// Noty js defaults
	Noty.overrideDefaults({
		type: 'success',
		timeout: 3000,
		progressBar: false,
		theme: 'sunset'
	});
}());