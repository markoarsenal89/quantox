/**
 * Global namespace
 */
var APP = APP || {};


APP.map = (function () {
	let self = {
		defaults: {},
		instance: {}
	}

	class Map {
		/**
		 * Constructor function
		 * @param { Jquery } 	$element 	Module element
		 * @param { Object } 	options  	Module options
		 */
		constructor ($element, options) {
			this.$element = $element;
			this.options = $.extend(true, {}, self.defaults, options);

			this.DOM = {
				$mapContainer: $element.find('.js-map-container')
			}

			this.route = {};

			this.map = null;
			this.directionsService = null;
			this.directionsRenderer = null;

			this._initModules();
			this._initEvents();

			this.getAndShowDirections();
		}

		getRouteFromStorage () {
			let hash = location.hash.replace('#', ''),
				routeId = -1;

			try {
				routeId = parseInt(hash);
			} catch (e) {
				new Noty({
					type: 'error',
					text: 'Round id is not a number'
				}).show();

				return false;
			}

			let routes = JSON.parse(localStorage.getItem('routes')) || {},
				route = routes[routeId];

			if (!route) {
				new Noty({
					type: 'error',
					text: 'There is no route with that id'
				}).show();
			}

			return route;
		}

		getAndShowDirections () {
			let route = this.getRouteFromStorage();

			if (route) {
				let start = route.start.geometry.location,
					end = route.end.geometry.location;

				window.request = {
					origin: new google.maps.LatLng(start.lat, start.lng),
					destination: new google.maps.LatLng(end.lat, end.lng),
					travelMode: google.maps.DirectionsTravelMode.DRIVING
				}

				this.directionsService.route(request, (response, status) => {
					if (status == google.maps.DirectionsStatus.OK) {
						let route = response.routes[0],
							routeCenter = route.overview_path[Math.floor(route.overview_path.length / 2)],
							routeCenterPosition = {
								lat: routeCenter.lat(),
								lng: routeCenter.lng()
							},
							distance = route.legs[0].distance.value,
							distanceFormatted = distance > 1000 ? Number.prototype.round.call(distance / 1000, 2) + 'km' : distance + 'm',
							duration = route.legs[0].duration.value,
							durationFormatted = duration.toHHMMSS();

						let infoWindow = new google.maps.InfoWindow({
							content: '<div class="route-info">\
										<label class="info-label">Distance: </label><span class="info-value">' + distanceFormatted + '</span><br />\
										<label class="info-label">Travel time: </label><span class="info-value">' + durationFormatted + '</span>\
									</div>',
							position: routeCenterPosition
						});

						this.directionsRenderer.setDirections(response);						
						infoWindow.open(this.map);
					} else {
						new Noty({
							type: 'error',
							text: 'Error, ' + status + ', please try again later'
						}).show();
					}
				});

				// stepDisplay = new google.maps.InfoWindow();
			}
		}

		/**
		 * Additional modules
		 * @return { void }
		 */
		_initModules () {
			this.map = new google.maps.Map(this.DOM.$mapContainer[0], {
				zoom: 12,
				center: { lat: 44.7865679, lng: 20.4489216 }
			});

			this.directionsService = new google.maps.DirectionsService();

			this.directionsRenderer = new google.maps.DirectionsRenderer({
				polylineOptions: {
					strokeColor: '#0275d8',
					strokeWeight: 4
				},
				map: this.map
			});
		}

		/**
		 * Events
		 * @return { void }
		 */
		_initEvents () {
			
		}
	}

	/**
	 * Init module
	 * @param  	{ String } 		selector 	Jquery selector
	 * @param 	{ Object } 		options 	Module options
	 * @return 	{ Array } 					Array of istances	
	 */
	self.init = function (selector, options) {
		let $elements = $(selector),
			results = [];

		// Add modul methods to every element
		$elements.each(function(index, el) {
			let $this = $(this),
				map = $this.data('map');

			if (!map) {
				map = new Map ($this, options);
				self.instance = map;
				$this.data('map', map);
			}
			
			results.push(map);
		});

		return results;
	}


	return self;
}());