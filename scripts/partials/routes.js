/**
 * Global namespace
 */
var APP = APP || {};


APP.routes = (function () {
	let self = {
		defaults: {},
		instance: {}
	}

	class Routes {
		/**
		 * Constructor function
		 * @param { Jquery } 	$element 	Module element
		 * @param { Object } 	options  	Module options
		 */
		constructor ($element, options) {
			this.$element = $element;
			this.options = $.extend(true, {}, self.defaults, options);

			this.DOM = {
				$form: $element.find('.js-form'),
				$routePointInput: $element.find('.js-route-point-input'),
				$routePointInput1: $element.find('.js-route-point-input-1'),
				$routePointInput2: $element.find('.js-route-point-input-2'),
				$addBtn: $element.find('.js-add-btn'),
				$routesList: $element.find('.js-routes-list'),
				$routeTemplate: $element.find('.js-route-template')
			}

			// Get routes
			this.routes = this.getRoutes();
			this.routesLength = 0;

			// Validator
			this.validator = null;

			// Route template
			let source = this.DOM.$routeTemplate.html();
			this.routeTemplate = Handlebars.compile(source);

			// Autocomplete instances
			this.autocomplete1 = null;
			this.autocomplete2 = null;

			this._initModules();
			this._initEvents();
		}

		/**
		 * Get routes from storage
		 * @return { Object } 	Object with routes
		 */
		getRoutes () {
			return JSON.parse(localStorage.getItem('routes')) || {};
		}

		/**
		 * Savet routes to storage
		 * @param  { Array } 	routes 	Array of routes
		 * @return { void }
		 */
		saveRoutes (routes) {
			localStorage.setItem('routes', JSON.stringify(routes));
		}

		/**
		 * Add route to the list
		 * @param { Object } 	start 	Google place
		 * @param { Object } 	end   	Google place
		 * @return { Object } 			Route object
		 */
		addRoute (start, end) {
			let id = this.routesLength + 1;
			let route = {
				name: start.formatted_address + ' - ' + end.formatted_address,
				start: start,
				end: end
			}
			
			this.routes[id] = route;
			this.routesLength = id;

			return route;
		}

		/**
		 * Render routes into DOM
		 * @param  { Array } 	routes 	Routes array
		 * @return { void }
		 */
		renderRoutes (routes) {
			let routesHtml = this.routeTemplate(routes);
			this.DOM.$routesList.html(routesHtml);
		}

		/**
		 * Reset form
		 * @return { void }
		 */
		resetForm () {
			this.DOM.$form[0].reset();
			this.DOM.$routePointInput1[0].focus();
		}

		/**
		 * Additional modules
		 * @return { void }
		 */
		_initModules () {
			// Init autocomplete
			this.autocomplete1 = new google.maps.places.Autocomplete(this.DOM.$routePointInput1[0], { types: ['(cities)'] });
			this.autocomplete2 = new google.maps.places.Autocomplete(this.DOM.$routePointInput2[0], { types: ['(cities)'] });

			// Render added routes
			let routes = this.getRoutes();

			this.renderRoutes(routes);
			for (let i in routes) {
				if (i === 'length') {
					continue;
				}

			    this.addRoute(routes[i].start, routes[i].end);
			}

			// Validate form
			this.validator = this.DOM.$form.validate({
				rules: {
					routePoint1: { validPlace: true },
					routePoint2: { validPlace: true }
				},
				onfocusout: false
			});
		}

		/**
		 * Events
		 * @return { void }
		 */
		_initEvents () {
			let that = this;

			// Prevent form submit
			this.DOM.$routePointInput.on('keydown', function (e) {
				e.keyCode === 13 ? e.preventDefault() : null;
			});

			// Remember autocomplete result
			google.maps.event.addListener(that.autocomplete1, 'place_changed', function () {
				that.DOM.$routePointInput1.data('place', that.autocomplete1 && that.autocomplete1.getPlace());
				that.validator.element(that.DOM.$routePointInput1);
			});

			google.maps.event.addListener(that.autocomplete2, 'place_changed', function () {
				that.DOM.$routePointInput2.data('place', that.autocomplete2 && that.autocomplete2.getPlace());
				that.validator.element(that.DOM.$routePointInput2);
			});

			// Add a route
			this.DOM.$addBtn.on('click', () => {
				if (this.DOM.$form.valid()) {
					let start = this.autocomplete1.getPlace(),
						end = this.autocomplete2.getPlace();

					this.addRoute(start, end);
					this.saveRoutes(this.routes);
					this.renderRoutes(this.routes);

					// Notification
					new Noty({ text: 'Route is successfully added' }).show();

					this.resetForm();
				}
			});

			// Remove a route
			this.$element.on('click', '.js-remove-btn', function () {
				let $round = $(this).closest('.js-route');

				$round.velocity({ scale: 0 }, function () {
					let roundId = $round.data('id');

					$round.remove();
					delete that.routes[roundId];

					that.saveRoutes(that.routes);
					that.renderRoutes(that.routes);

					// Notification
					new Noty({ text: 'Route is successfully deleted' }).show();
				}, 200);
			});
		}
	}

	/**
	 * Init module
	 * @param  	{ String } 		selector 	Jquery selector
	 * @param 	{ Object } 		options 	Module options
	 * @return 	{ Array } 					Array of istances	
	 */
	self.init = function (selector, options) {
		let $elements = $(selector),
			results = [];

		// Add modul methods to every element
		$elements.each(function(index, el) {
			let $this = $(this),
				routes = $this.data('routes');

			if (!routes) {
				routes = new Routes ($this, options);
				self.instance = routes;
				$this.data('routes', routes);
			}
			
			results.push(routes);
		});

		return results;
	}


	return self;
}());